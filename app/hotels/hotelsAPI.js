const request = require('request'); 
const PriceRangeError = require('./hotelsError')
const DateRangeError = require('./hotelsError')
var url = require('url');


module.exports = function(app) {
  app.get('/hotels', (req, response) => {
    // You'll create your note here.
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    getHotels('https://api.myjson.com/bins/tl0bp', function(err, hotelsList){
    if (err) {
      console.log(err);
    }
    else {
      try{
      filterPipeline.filter(hotelsList, query)
      response.send(hotelsList.hotels)
    }
    catch(error){
      console.log('****************************')
      console.log(JSON.stringify(error))
      console.log('****************************')
      response.send(error)
    }
  }
  });
});
};

function getHotels(url, callback) {
  request({
    url: url,
    json: true
  }, function (error, response, body) {
    if (error || response.statusCode !== 200) {
      return callback(error || {statusCode: response.statusCode});
    }
    callback(null, body);  
  });
}


//Chain of responsibility design pattern


//Use filterPipeline.filter(request)
//to send the request to be handled along the
//chain-of-responsibility
var filterPipeline = {
  filter: function(hotelsList, query){
    var nameFilter = new HotelNameFilter();
    var cityFilter = new HotelCityFilter();
    var priceFilter = new PriceRangeFilter();
    var dateFilter = new DateRangeFilter();

    nameFilter.setNext(cityFilter).setNext(priceFilter).
      setNext(dateFilter);
    nameFilter.filter(hotelsList, query);
  }
}


//Handler
var Filter = function(){
  this.next = {
    filter: function(hotelsList, query){
      console.log('All strategies exhausted.');
    }
  }
};
Filter.prototype = {
  setNext: function(next){
    this.next = next;
    return next;
  },
  filter: function(hotelsList, query){}
}



//HotelNameFilter
var HotelNameFilter = function(){};
HotelNameFilter.prototype = new Filter();
HotelNameFilter.prototype.filter = function(hotelsList, query){
  var newArray;
  if('name' in query){
    hotelsList.hotels = hotelsList.hotels.filter(hotel => hotel.name == query.name);
  }
  this.next.filter(hotelsList, query);
}



//HotelCityFilter
var HotelCityFilter = function(){};
HotelCityFilter.prototype = new Filter();
HotelCityFilter.prototype.filter = function(hotelsList, query){
  if('city' in query){
    hotelsList.hotels = hotelsList.hotels.filter(
      hotel => hotel.city == query.city); 
  }
  this.next.filter(hotelsList, query);
}



//PriceRangeFilter
var PriceRangeFilter = function(){};
PriceRangeFilter.prototype = new Filter();
PriceRangeFilter.parse = function(query){
  var result = {};
  var minPrice, maxPrice;
  if('min_price' in query && 'max_price' in query){
    minPrice = parseInt(query.min_price);
    maxPrice = parseInt(query.max_price);
    result.status = true;
  }
  else if('min_price' in query){
    minPrice = parseInt(query.min_price);
    maxPrice = null;
    result.status = true;
  }
  else if('max_price' in query){
    minPrice = 0;
    maxPrice = parseInt(query.max_price);
    result.status = true;
  }
  else{
    result.status = false; 
  }
  result.minPrice = minPrice;
  result.maxPrice = maxPrice;
  return result;
};
PriceRangeFilter.validate = function(minPrice, maxPrice){
  if(!TypeValidation.isNumber(minPrice) || isNaN(minPrice)){
    throw new PriceRangeError('Minimum price is not integer');
  }
  if(maxPrice != null){
    if(!TypeValidation.isNumber(maxPrice) || isNaN(maxPrice)){
      throw new PriceRangeError('Maximum price is not integer');
    }
    if(minPrice > maxPrice){
      throw new PriceRangeError('Invalid price range values');
    }
  }
};
PriceRangeFilter.prototype.filter = function(hotelsList, query){
  var result = PriceRangeFilter.parse(query);
  if(result.status){
    PriceRangeFilter.validate(result.minPrice, result.maxPrice);
    if(result.maxPrice != null){
      hotelsList.hotels = hotelsList.hotels.filter(
        hotel => hotel.price >= result.minPrice &&
        hotel.price <= result.maxPrice);
    }
    else{
      hotelsList.hotels = hotelsList.hotels.filter(hotel => hotel.price >= result.minPrice);
    }
  }
  this.next.filter(hotelsList, query);
}



//DateRangeFilter
var DateRangeFilter = function(){};
DateRangeFilter.prototype = new Filter();
DateRangeFilter.ISOFormat = function(date){
  var splitedDate = date.split("-");
  if(splitedDate.length != 3){
    throw new DateRangeError('Invalid date format.')
  }
  var day = splitedDate[0];
  splitedDate[0] = splitedDate[1];
  splitedDate[1] = day;
  return splitedDate.join('-');
};
DateRangeFilter.parse = function(startDate, endDate){
  if(startDate != null && endDate != null){
    startDate = new Date(DateRangeFilter.ISOFormat(startDate));
    endDate = new Date(DateRangeFilter.ISOFormat(endDate));
    return {'startDate': startDate, 'endDate': endDate};
  }
  throw new DateRangeError('Missing start or end date.');
};
DateRangeFilter.validate = function(startDate, endDate){
  if(isNaN(startDate.valueOf())){
    throw new DateRangeError('Invalid start date value');
  }
  if(isNaN(endDate.valueOf())){
    throw new DateRangeError('Invalid end date value');
  }
  
  if(startDate > endDate){
    throw new DateRangeError('Invalid date range values');
  }
};
DateRangeFilter.prototype.filter = function(hotelsList, query){
  if('start_date' in query || 'end_date' in query){
    var startDate = query.start_date || null;
    var endDate = query.end_date || null;
    var dateDict = DateRangeFilter.parse(startDate, endDate);
    DateRangeFilter.validate(dateDict.startDate, dateDict.endDate);
    hotelsList.hotels = hotelsList.hotels.filter(function(hotel){
      return hotel.availability.some(function(duration){
        var durationDates = DateRangeFilter.parse(duration.from, duration.to);
        return (dateDict.startDate >= durationDates.startDate &&
          dateDict.endDate <= durationDates.endDate);
      })
    });
  }
  this.next.filter(hotelsList, query);
}


class TypeValidation {
  static isNumber(value) {
    return (typeof(value) == 'number') ? true : false;
  }
}