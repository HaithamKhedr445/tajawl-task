//Chain of responsibility design pattern


//Use strategyPipeline.handleRequest(request)
//to send the request to be handled along the
//chain-of-responsibility
var strategyPipeline = {
  handleRequest: function(request){
    var strategy1 = new Strategy1();
    var strategy2 = new Strategy2();
    var strategy3 = new Strategy3();
    var strategy4 = new Strategy4();

    strategy1.setNext(strategy2).setNext(strategy3).setNext(strategy4);
    strategy1.handleRequest(request);
  }
}


//Handler
var Handler = function(){
  this.next = {
    handleRequest: function(request){
      console.log('All strategies exhausted.');
    }
  }
} ;
Handler.prototype.setNext = function(next){
  this.next = next;
  return next;
}
Handler.prototype.handleRequest = function(request){};

//Strategy1
var Strategy1 = function(){};
Strategy1.prototype = new Handler();
Strategy1.prototype.handleRequest = function(request){
  console.log('Strategy1');
  if(request == 1){
    return ;
  }
  this.next.handleRequest(request);
}

//Strategy2
var Strategy2 = function(){};
Strategy2.prototype = new Handler();
Strategy2.prototype.handleRequest = function(request){
  console.log('Strategy2');
  if(request == 2){
    return ;
  }
  this.next.handleRequest(request);
}

//Strategy3
var Strategy3 = function(){};
Strategy3.prototype = new Handler();
Strategy3.prototype.handleRequest = function(request){
  console.log('Strategy3');
  if(request == 3){
    return ;
  }
  this.next.handleRequest(request);
}

//Strategy4
var Strategy4 = function(){};
Strategy4.prototype = new Handler();
Strategy4.prototype.handleRequest = function(request){
  console.log('Strategy4');
  if(request == 4){
    return ;
  }
  this.next.handleRequest(request);
}

strategyPipeline.handleRequest(5)



Subway.sixInch = function (subwayObj) { //Method on Subway function object
  var cost = subwayObj.cost(); //Get current cost of sandwich
    subwayObj.cost = function () { //We aren't changing interface but providing more functionality
      return cost - 1;
    }
}    
 
Subway.footLong = function (subwayObj) {}; //Default is Foot Long 
Subway.partySub = function (subwayObj) { 
  var cost = subwayObj.cost(); 
    subwayObj.cost = function () { 
      return cost + 3;
    }
}

Subway.partySub = function (subwayObj) { 
  var cost = subwayObj.cost(); 
    subwayObj.cost = function () { 
      return cost + 3;
    }
}

Subway.pickle = function (subwayObj) { 
  var cost = subwayObj.cost(); 
    subwayObj.cost = function () { 
      return cost + 0.12;
    }
}

Subway.mustard = function (subwayObj) { 
  var cost = subwayObj.cost(); 
    subwayObj.cost = function () { 
      return cost + 0.25;
    }
}

Subway.ketchup = function (subwayObj) { 
  var cost = subwayObj.cost(); 
    subwayObj.cost = function () { 
      return cost + 0.10;
    }
}




//Chain of responsibility design pattern


//Use strategyPipeline.handleRequest(request)
//to send the request to be handled along the
//chain-of-responsibility
var validationPipeline = {
  validate: function(data){
    var priceRangeValidation = new PriceRangeValidation();
    var dateRangeValidation = new DateRangeValidation();

    priceRangeValidation.setNext(dateRangeValidation);
    priceRangeValidation.validate(data);
  }
}


//Handler
var Validation = function(){
  this.next = {
    validate: function(data){
      console.log('All validation exhausted.');
    }
  }
} ;
Validation.prototype.setNext = function(next){
  this.next = next;
  return next;
}
Validation.prototype.validate = function(data){};

//PriceRangeValidation
var PriceRangeValidation = function(){};
PriceRangeValidation.prototype = new Validation();
PriceRangeValidation.prototype.validate = function(data){
  if(request == 1){
    return ;
  }
  this.next.validate(data);
}

//DateRangeValidation
var DateRangeValidation = function(){};
DateRangeValidation.prototype = new Validation();
DateRangeValidation.prototype.validate = function(data){
  if(request == 2){
    return ;
  }
  this.next.validate(data);
}

validationPipeline.validate(1)